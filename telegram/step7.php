<?php
$is_success = false;
$is_failed = false;

if (!empty($_POST)) {
    $name = $_POST['name'] ?? '';
    $name = preg_replace('/[^A-Za-zА-яа-я ]/ui', '', $name);

    $phone = $_POST['phone'] ?? '';
    $phone = preg_replace("/[^0-9]/", '', $phone);

    if (!empty($name) && !empty($phone)) {
        $message = "Новый заказ\n";
        $message .= "Имя: " . $name . "\n";
        $message .= "Номер телефона: " . $phone;

        $chat_id = '5498313873';
        $api_token = '5448429020:AAFRJ_z3kce6Mh6Q_NrEQFC6sreOuMO7X9Q';
        $url = 'https://api.telegram.org/bot' . $api_token . '/sendMessage';

        $body = [
            'chat_id' => $chat_id,
            'text' => $message,
            'parse_mode' => 'HTML',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $response = curl_exec($ch);

        $is_success = true;
    } else {
        $is_failed = true;
    }
}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Оформление заказа</title>
</head>

<body>
    <div class="container pt-5">
        <h1 class="text-center">Оформление заказа</h1>
        <form class="row" method="post">
            <div class="col-md-6 offset-md-3">
                <div class="form-group my-3">
                    <label for="name" class="form-label">Ваше имя</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group my-3">
                    <label for="phone" class="form-label">Ваш номер телефона</label>
                    <input type="text" class="form-control" id="phone" name="phone" required>
                </div>
                <div class="form-group my-3">
                    <button class="btn btn-outline-success" type="submit">Отправить</button>
                </div>
            </div>
            <?php if ($is_success) { ?>
                <div class="my-3 text-center">
                    <p class="lead text-success">Ваш заказ успешно оформлен</p>
                </div>
            <?php } ?>

            <?php if ($is_failed) { ?>
                <div class="my-3 text-center">
                    <p class="lead text-danger">Одно или несколько полей некорректны</p>
                </div>
            <?php } ?>
        </form>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>