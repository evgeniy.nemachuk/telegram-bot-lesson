<?php
// ========== get Webhook ==========
// https://core.telegram.org/bots/api#setwebhook

$input = file_get_contents('php://input');
$request = json_decode($input, true);

if (empty($request['message'])) {
    exit;
}

$chat_id = $request['message']['from']['id'] ?? 0;
$text = mb_strtolower($request['message']['text'] ?? '');

if (empty($chat_id) || empty($text)) {
    exit;
}

switch ($text) {
    case 'hello':
        $full_name = getName($request['message']);
        $message = "Hello, " . $full_name . "\nHow are you?";
        sendMessage($chat_id, $message);
        break;
    case 'fine':
        $message = "Excellent! I'm happy for you!";
        sendMessage($chat_id, $message);
        break;
    case 'good':
        $message = "Great. Continue as well!";
        sendMessage($chat_id, $message);
        break;
    case 'bad':
        $message = "You need to cheer up!";
        sendMessage($chat_id, $message);
        break;
    default:
        $message = "I dont understand";
        sendMessage($chat_id, $message);
        break;
}

function sendMessage($chat_id, $message)
{
    $api_token = '5448429020:AAFRJ_z3kce6Mh6Q_NrEQFC6sreOuMO7X9Q';
    $url = 'https://api.telegram.org/bot' . $api_token . '/sendMessage';

    $body = [
        'chat_id' => $chat_id,
        'text' => $message,
        'parse_mode' => 'HTML',
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    $response = curl_exec($ch);

    return $response;
}

function getName($message)
{
    $first_name = '';
    $last_name = '';

    if (!empty($message['from']['first_name'])) {
        $first_name = $message['from']['first_name'];
    }

    if (!empty($message['from']['last_name'])) {
        $last_name = $message['from']['last_name'];
    }

    return $first_name . ' ' . $last_name;
}
