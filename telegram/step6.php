<?php

try {
    //$mysql = dataBaseConnection();

    // require header 
    if (file_exists('header.php')) {
        require 'header.php';
    }

    // require main page 
    if (file_exists('main.php')) {
        require 'main.php';
    }

    // require anyfile 
    if (file_exists('anyfile.php')) {
        require 'anyfile.php';
    }

    echo 'some other logic <br>';
    echo 'some other logic <br>';
    echo 'some other logic <br>';

    // require footer 
    if (file_exists('footer.php')) {
        require 'footer.php';
    }

    $number = 10;
    $zero = 0;
    $result = $number / $zero;

} catch (Throwable $e) {
    sendError($e);
}

function dataBaseConnection()
{
    $mysql = mysqli_connect('example.com:3307', 'mysql_user', 'mysql_password');
    return $mysql;
}

function sendError($e)
{
    $message = "Error caught\n";
    $message .= "Message: " . $e->getMessage() . "\n";
    $message .= "File: " . $e->getFile() . "\n";
    $message .= "Line: " . $e->getLine() . "\n";

    $chat_id = '5498313873';
    $api_token = '5448429020:AAFRJ_z3kce6Mh6Q_NrEQFC6sreOuMO7X9Q';
    $url = 'https://api.telegram.org/bot' . $api_token . '/sendMessage';

    $body = [
        'chat_id' => $chat_id,
        'text' => $message,
        'parse_mode' => 'HTML',
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    $response = curl_exec($ch);

    return $response;
}
