<?php
$urls = [
    'https://www.letu.ru/storeru/product/dior-homme-sport./52400009/sku/66000004?pushSite=storeMobileRU&format=json',
    // 'https://www.letu.ru/storeru/product/dior-sauvage/61700031?pushSite=storeMobileRU&format=json',
    // 'https://www.letu.ru/storeru/product/givenchy-gentleman-eau-de-toilette-intense/98300011?pushSite=storeMobileRU&format=json'
];

foreach ($urls as $url) {
    $file = file_get_contents($url);

    $json = json_decode($file, true);

    if (empty($json['contents'][0]['mainContent'][0]['contents'][0]['productContent'][0])) {
        exit('empty');
    }

    $link = $json['canonical'] ?? 'no link';

    $product = $json['contents'][0]['mainContent'][0]['contents'][0]['productContent'][0]['product'] ?? [];

    $product_name = $product['displayName'] ?? 'no name';
    $message = "Letu.ru / " . $product_name . "\n";
    $price_list = $json['contents'][0]['mainContent'][0]['contents'][0]['productContent'][0]['skuList'] ?? [];

    foreach ($price_list as $list) {
        $display_name = $list['displayName'] ?? 'no display name';
        $price = $list['price']['amount'] ?? 'no price';

        $message .= $display_name . ": <b>" . $price . "р</b>\n";
    }

    $message .= $link;

    foreach (str_split($message, 2000) as $string) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:multipart/form-data"
        ));
        curl_setopt($ch, CURLOPT_URL, 'https://api.telegram.org/bot5448429020:AAFRJ_z3kce6Mh6Q_NrEQFC6sreOuMO7X9Q/sendMessage');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['chat_id' => '5498313873', 'parse_mode' => 'HTML', 'text' => $string]);
        curl_exec($ch);
    }
    sleep(1);
}
